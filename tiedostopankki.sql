/**/
drop database if exists tiedostopankki;

create database tiedostopankki;

use tiedostopankki;

CREATE TABLE `tiedostot` (
 `id` int NOT NULL AUTO_INCREMENT,
 `tiedostonimi` varchar(255) NOT NULL,
 `thumb` varchar(255) NOT NULL,
 `tallennettu` datetime NOT NULL,
 `kuvaus` varchar(255) NOT NULL,
 `nimi` varchar(255) NOT NULL,
 PRIMARY KEY (`id`)
);






