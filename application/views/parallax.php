
<head>
<style>
body, html {
  height: 100%;
  margin: 0;
  font: 400 15px/1.8 "Lato", sans-serif;
  color: #777;
}

body {
      position: relative; 
  }

.bgimg-1, .bgimg-2, .bgimg-3 {
  position: relative;
  opacity: 0.75;
  background-attachment: fixed;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;

}
.bgimg-1 {
  background-image: url("http://localhost/ci_rautapojat_template/uploads/bg_texture1.jpg");
  min-height: 400px;
}

.bgimg-2 {
  background-image: url("http://localhost/ci_rautapojat_template/uploads/Steel-Industry-Electric-Motors-Industrial-Fan-Blowers-Stiavelli-FTA-3.jpg");
  min-height: 400px;
}

.bgimg-3 {
  background-image: url("img_parallax3.jpg");
  min-height: 400px;
}

.caption {
  position: absolute;
  left: 0;
  top: 30%;
  width: 100%;
  text-align: center;
  color: #000;
}

.caption2 {
  position:relative;
  text-align: center;
  color: #000;
  padding-bottom: 20px;
}

.caption span.border {
  background-color: #111;
  color: #fff;
  padding: 18px;
  font-size: 25px;
  letter-spacing: 10px;
}

h3 {
  letter-spacing: 2px;
/*  text-transform: uppercase;*/
  font: 17px "Lato", sans-serif;
  color: #111;
}

/* Turn off parallax scrolling for tablets and phones */
@media only screen and (max-device-width: 1024px) {
    .bgimg-1, .bgimg-2, .bgimg-3 {
        background-attachment: scroll;


.caption.logo {
  -webkit-filter: drop-shadow(10px 10px 10px 10px #999);
  filter: drop-shadow(10px 10px 10px 10px #999);
}

.carousel img {
  position: absolute;
  top: 0;
  left: 0;
  min-width: 100%;
  height: 500px;
}
.fluidMedia {
    position: relative;
    padding-bottom: 56.25%; /* proportion value to aspect ratio 16:9 (9 / 16 = 0.5625 or 56.25%) */
    padding-top: 0px;
    height: 50%;
    overflow: hidden;
}

.fluidMedia iframe {
    position: absolute;
    top: 0; 
    left: 0;
    width: 100%;
    height: 100%;
}



</style>
</head>


<div class="bgimg-1">
    
  <div class="caption">
    <img class="logo" src="<?php echo base_url('/uploads/logo.png');?>"width="18%" height="18%">
    <span class="border">OULUN RAUTA-POJAT OY</span>
  </div>
</div>
<div id="section1" class="container-fluid">
    <div style="color: #777;background-color:white;padding:10px 20px;text-align: justify;">
    <h3 style="text-align:center;">Olemme Oulun Ruskossa toimiva tilauskonepaja, joka valmistaa asiakkailleen ruostumattomia/Fe-teräsrakenteita
      sekä Al-rakenteita asiakkaan mittapiirustusten ja tarpeiden mukaisesti.</h3></tr>
<h3 style="text-align:center;">Konepajallamme on käytössä kantavien rakenteiden CE-merkintästandardi SFS-EN 1090-1+A1 toteutusluokissa EXC1 ja EXC2.</h3>
  

    
</div>
    <div class="caption2">
    <img class="logo" src="<?php echo base_url('uploads/logot.png');?>"width="60%" height="60%">
    </div>


    
  </div>


  
        
<div class="container-fluid" id="section2">	
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
      
    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    <?php 
    $a = 1;
        for ($a = 1; $a < $numrows; $a++) {
        ?><li data-target="#carousel-example-generic" data-slide-to="<?php echo $a?>"></li><?php
    }
    
    ?>
    
  </ol>


  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox" style=" width:100%; height:400px !important;">

    <?php
    $i  = 1;
    foreach ($files as $file) {
        $item_class = ($i == 1) ? 'item active' : 'item';?>
        <div class="<?php echo $item_class; ?>">
            
                <img src="<?php echo base_url('uploads/'.$file['tiedostonimi']);?>"/>
           
        <div class="carousel-caption">
            <h1><?php echo $file['kuvaus'];?></h1>
        </div>
</div>
<?php
    $i++;
}
?> 
</div>   
  <div>   

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
<!--  <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>-->
</div>
    </div>
</div>

        
  

<div style="position:relative;">
    <div class="overlay" onClick="style.pointerEvents='none'">
        <div style="color:#ddd;background-color:#282E34;text-align:center;padding:10px 10px;text-align: justify;">
            <div class="fluidMedia">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1682.7997213232816!2d25.537433678458907!3d65.05339198284786!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46802d2c64e417d3%3A0x3519b1e9ed7e108!2sOulun+Rauta-Pojat+Oy!5e0!3m2!1sen!2sfi!4v1493122032963"></iframe>
        </div>
   </div>
      
   
		

  </div>
</div>

<div id="section3" class="bgimg-2">
  <div class="caption">
    <span class="border" style="background-color:transparent;font-size:25px;color: #f7f7f7;">
    

    </span>
  </div>
</div>

<div style="color: #777;background-color:white;text-align:center;padding:50px 80px;text-align: justify;">
    
    <form class="form-horizontal">
        <div class="form">
            <fieldset>

<!-- Form Name -->
<legend><h3 style="text-align:center;">Kiinnostuitko?</br> Ota yhteyttä alapuolella olevalla lomakkeella.</br> Voit myös jättää avoimen työhakemuksen.</h3>
</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Nimi:</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="Matti Möttönen" class="form-control input-md" required="">
<!--  <span class="help-block">help</span>  -->
  </div>
</div>
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">Email:</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="joku@jossain.fi" class="form-control input-md" required="">
<!--  <span class="help-block">help</span>  -->
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="radios">Aihe:</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="radios-0">
      <input type="radio" name="radios" id="radios-0" value="1" checked="checked">
      Tuotteet
    </label>
	</div>
  <div class="radio">
    <label for="radios-1">
      <input type="radio" name="radios" id="radios-1" value="2">
      Työhakemus
    </label>
	</div>
  </div>
</div>

<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="textarea">Viesti:</label>
  <div class="col-md-4">                     
    <textarea class="form-control" id="textarea" name="textarea" placeholder="...."></textarea>
  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="singlebutton"></label>
  <div class="col-md-4">
    <button id="singlebutton" name="singlebutton" class="btn btn-primary">Lähetä</button>
  </div>
</div>
    </fieldset>
    </div>
    </form>
  

    <div class="container container-fluid">
      <!-- Example row of columns -->
      <div class="row">
        <div class="col-md-4" style="text-align: center;">
          <h2><span class="glyphicon glyphicon-globe"/></h2>
          <p>Oulun Rauta-Pojat Oy</p>
        </div>
          <div class="col-md-4" style="text-align: center;">
          <h2><span class="glyphicon glyphicon-earphone"/></h2>
          <p>08 5568671</p>
        </div>
          <div class="col-md-4" style="text-align: center;">
          <h2><span class="glyphicon glyphicon-map-marker"/></h2>
          <p>Rusko, Oulu</br>
            Liuskekuja 5</br>
            90630 Oulu</p>
        </div>
        
        </div>
      </div>
        </div>
      

      

<div class="bgimg-1">
  <div class="caption">
    <span class="border">COOL!</span>
  </div>
</div>
</div>
<script>
 $(document).ready(function(){
     
  // Add scrollspy to <body>
  $('body').scrollspy({target: ".navbar", offset: 50});   

  // Add smooth scrolling on all links inside the navbar
  $("#myNavbar a").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
   
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    }  // End if
  });
});
 </script>

