<div class="row">
    <div class="col-xs-12 col-md-offset-4 col-md-4">        
        <h3>Rekisteröidy</h3>
        
        <form role="form" method="post" action="<?php print site_url(). '/galleria/rekisteroidy'?>">
            <div class="form-group">
                <label for="tunnus">Tunnus:</label>
                <input type="text" name="tunnus" class="form-control">
            </div>
            <div class="form-group">
                <label for="salasana">Salasana:</label>
                <input type="password" name="salasana" class="form-control">
            </div>
            <div class="form-group">
                <label for="salasana2">Salasana uudestaan:</label>
                <input type="password" name="salasana2" class="form-control">
            </div>
            <button class="btn btn-primary">Tallenna</button>
            <a class="btn btn-default" href="<?php print site_url() . '/galleria/index'?>">
                Kirjaudu
            </a>
        </form>
    </div>
</div>