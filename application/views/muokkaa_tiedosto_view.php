<div class="container">
    <h3>Lisää kuva</h3>
    <?php
        print validation_errors("<p style='color: red'>","</p>");
        ?>
    <div class="row">
        <p><?php echo $this->session->flashdata('statusMsg'); ?></p>
        <form action="<?php print site_url() . 'tiedosto/tallenna';?>" method="post">
            <input type="hidden" name="id" value="<?php print $muokattava->id;?>">
            <div class="form-group">
                <label>Nimi</label>
                <input name="nimi" class="form-control" value="<?php print $muokattava->nimi;?>">
                <?php print form_error('nimi');?>
            </div>
            <div class="form-group">
                <label>Valitse tiedosto</label>
                <input type="file" class="form-control" name="userFile" /><span><?php print $muokattava->tiedostonimi?></span>
            </div>
            <div class="form-group">
                <label>Kuvaus</label>
                <input name="kuvaus" class="form-control"value="<?php print $muokattava->kuvaus;?>">
                
            </div>
            <div class="form-group">
                <button class="btn btn-primary" type="submit" name="fileSubmit" value="Muokkaa"/>
                Muokkaa</button>
                <a class="btn btn-default" href="<?php print site_url() . 'tiedosto'?>">
                Palaa
            </a>
            </div>
        </form>
    </div>  
</div>
