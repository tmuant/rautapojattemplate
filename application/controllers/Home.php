<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function __construct() {
        parent::__construct();        
 
                $this->load->helper(array('form', 'url'));
                $this->load->model('tiedosto_model');
    }
    
    public function index() {
        $data['title'] = 'Rautapoijat';
        $data['files'] = $this->tiedosto_model->get_rows();
        $data['numrows'] = $this->tiedosto_model->laske_tiedostot();
        
//        $this->load->view('template/header',$data);
        $this->load->view('template/nav_1_1');
        $this->load->view('parallax', $data);
        $this->load->view('template/footer');
    }
    
    
}












