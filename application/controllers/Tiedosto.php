<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto extends CI_Controller {
        public function __construct() {
                parent::__construct();
                
                if (!isset($_SESSION['kayttaja'])) {
                    redirect('galleria');
                }
              
                $this->load->library('pagination');
                $this->load->library('form_validation');
                $this->load->helper(array('form', 'url'));
                $this->load->model('tiedosto_model');
                $this->load->library('util');
                
            
        }
    
	public function index() {

                
        //Get files data from database
       $data['files'] = $this->tiedosto_model->get_rows();
        //Pass the files data to view
        $this->load->view('template/header');
        $this->load->view('template/nav');
        $this->load->view('tiedostot_view', $data);
        $this->load->view('template/footer');
    }
    
    public function lisaa(){
        
        if($this->input->post('fileSubmit')){
            $this->form_validation->set_rules('nimi','nimi','required');
            
            
            if ($this->form_validation->run() === TRUE) {
            
                $uploadPath = 'uploads/';
                $config['upload_path'] = $uploadPath;
                $config['allowed_types'] = 'gif|jpg|jpeg|bmp|png';
                
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){
                    $fileData = $this->upload->data();
                    $config['image_library'] = 'gd2';
                    $config['source_image'] = 'uploads/'.$fileData['file_name'];
                    $config['create_thumb'] = TRUE;
                    $config['maintain_ratio'] = TRUE;
                    $config['width']         = 300;
                    $config['height']       = 300;

                    $this->load->library('image_lib', $config);

                    $this->image_lib->resize();
                    $data = $this->upload->data();
                    
                    $uploadData['tiedostonimi'] = $fileData['file_name'];
                    $uploadData['thumb'] = $data['raw_name'].'_thumb'.$data['file_ext'];
                    $uploadData['tallennettu'] = date("Y-m-d H:i:s");
                    $uploadData['kuvaus'] = $this->input->post('kuvaus');
                    $uploadData['nimi'] = $this->input->post('nimi');
                    
                    
                } 
                    
                } 
            
            if(!empty($uploadData)){
                //Insert file information into the database
                $insert = $this->tiedosto_model->lisaa($uploadData);
                $statusMsg = $insert?'Tiedosto lisätty.':'Tiedoston lataaminen ei onnistunut.';
                $this->session->set_flashdata('statusMsg',$statusMsg);
            }
        }
        $this->load->view('template/nav');
        $this->load->view('template/header');
        $this->load->view('lisaa_tiedosto_view');
        $this->load->view('template/footer');
        
    }
    
    public function tallenna(){
        
        if($this->input->post('fileSubmit')){
            $this->form_validation->set_rules('nimi','nimi','required');
            
            
            if ($this->form_validation->run() === TRUE) {
            
                    $uploadData['id'] = $this->input->post('id');
                    $uploadData['kuvaus'] = $this->input->post('kuvaus');
                    $uploadData['nimi'] = $this->input->post('nimi'); 
                } 
                    
        }
            
            if(!empty($uploadData)){
                //Update information into the database
                $insert = $this->tiedosto_model->muokkaa($uploadData);
                $statusMsg = $insert?'Tiedosto lisätty.':'Tiedoston lataaminen ei onnistunut.';
                $this->session->set_flashdata('statusMsg',$statusMsg);
            
        
        redirect('tiedosto/index');
        }
    }
    public function muokkaa($id) {
                $data['muokattava'] = $this->tiedosto_model->hae($id);

                $this->load->view('template/nav');
                $this->load->view('template/header');
                $this->load->view('muokkaa_tiedosto_view',$data);
                $this->load->view('template/footer');
                        
                
        }

    public function carousel(){
        $data['files'] = $this->tiedosto_model->get_rows();
        $data['numrows'] = $this->tiedosto_model->laske_tiedostot();
        
        $this->load->view('template/nav');
        $this->load->view('template/header');
        $this->load->view('carousel_view', $data);
        $this->load->view('template/footer');
    }
    public function poista($id) {
            $this->tiedosto_model->poista(intval($id));
            //$this->index();
            redirect('tiedosto/index');
        }
        
        
    

}


