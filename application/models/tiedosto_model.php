<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tiedosto_Model extends CI_Model {



    
        public function get_rows($id = ''){
        $this->db->select('*');
        $this->db->from('tiedostot');
        if($id){
            $this->db->where('id',$id);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{
            $this->db->order_by('tallennettu','desc');
            $query = $this->db->get();
            $result = $query->result_array();
        }
        return !empty($result)?$result:false;
    }
    
    public function hae($id) {
                $this->db->where('id',$id);
                $query = $this->db->get('tiedostot');
                return $query->row();
        }
    
    
    
    
    public function laske_tiedostot()
        {
            
            
            return $this->db->count_all_results('tiedostot');
        }
    
    public function insert($data){
        $insert = $this->db->insert('tiedostot',$data);
        return $insert?true:false;
    }
    
    public function muokkaa($data) {
                $this->db->where('id',$data['id']);
                $this->db->update('tiedostot',$data);
        }
    
    public function poista($id) {
        $file = "SELECT tiedostonimi, thumb FROM tiedostot WHERE id= $id";
        $remove = $this->db->query($file);        
        $tiedosto=$remove->row();
        unlink("uploads/".$tiedosto->tiedostonimi);
        unlink("uploads/".$tiedosto->thumb);
        $this->db->where('id',$id);
        $this->db->delete('tiedostot');
        }
    
}